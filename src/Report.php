<?php

namespace Ifnloop\Report;
use GuzzleHttp\Client;
class Report
{

    public function setReport(array $content)
    {
        $responser  = [];
        $reworkContent = [];
        $error      = false;
        $requiredParams     = "name,description,text_content,priority";
        $arrayRequired      = explode(",", $requiredParams);

        foreach($arrayRequired as $s){
            if(!empty($content[$s])){
                $reworkContent[$s] = $content[$s];
            }else{
                $error = true;
            }

        }

        if(!$error){
            return $this->createTask($reworkContent);
        }
        $responser['error'] = true;
        $responser['args'] = "you missing required parameters : name,description,text_content,priority";
        return $responser;
    }

    private function createTask(array $workedArray)
    {
        $responser = [];
        $token = env('IFNLOOP_REPORT_TOKEN');
        $assignees = explode(',',env('IFNLOOP_REPORT_ASSIGNEES'));
        $folder = env('IFNLOOP_REPORT_FOLDER');

        //Check required parameter
        if(empty($token) || count($assignees) == 0 || empty($folder)){
            $responser['error'] = true;
            $responser['args'] = "No token or assignment or folder id please check our .env";
            return $responser;
        }

        $workedArray['assignees'] = $assignees;

        try{

            $client     = new Client([
                'base_uri' => 'https://api.clickup.com/api/v2/',
                'timeout' => 3.0,
            ]);
            $headers    = ['headers' => [
                'Authorization' => $token,
            ]];

            $body       = json_encode($workedArray);

            /*$client->request('POST','list/'.$folder."/task", ['headers' => ['Authorization' => $token]], [
                'body' => $body
            ]);*/
           $res =  $client->request('POST','list/'.$folder."/task",[
                'headers' => [
                    'Authorization' => $token,
                    'Content-Type' => 'application/json'
                ],
                'body' => $body
            ]);


           if($res->getStatusCode() == 200){
               $responser['error'] = false;
               $responser['args'] = "Task created";
               return $responser;
           }
        }catch(\Exception $e){
            $responser['error'] = true;
            $responser['args'] = $e->getMessage();
            return $responser;
        }


    }
}
